include(../module_defaults.pri)

QT += core gui xml widgets

CONFIG += c++11

TARGET = workspaceGUI
#CONFIG += console
CONFIG -= app_bundle

INCLUDEPATH  +=../../Trodes/src-config
INCLUDEPATH  +=../../Trodes/src-main

#create copy commands for the file
#platform specific copy commands DeviceProfiles.xml
macx: {
    copy_DeviceProfiles.commands = cp $$quote($$shell_path($$PWD/../../Resources/DeviceProfiles.xml)) $$quote($$shell_path($$DESTDIR/workspaceGUI.app/Contents/Resources))
}
unix:!macx {
#    copy_DeviceProfiles.commands = cp $$quote($$shell_path($$PWD/../../Resources/DeviceProfiles.xml)) $$quote($$shell_path($$TRODES_BIN_INSTALL_DIR/Resources))
RESOURCES_COPY_PATH = $$TRODES_BIN_INSTALL_DIR/Resources
resources.path = $$RESOURCES_COPY_PATH
resources.files = $$PWD/../../Resources/DeviceProfiles.xml
INSTALLS += resources

}
win32: {
    copy_DeviceProfiles.commands = $(COPY) $$quote($$shell_path($$PWD/../../Resources/DeviceProfiles.xml)) $$quote($$shell_path($$DESTDIR/Resources))
}
    QMAKE_EXTRA_TARGETS += copy_DeviceProfiles

    first.depends = $(first) copy_DeviceProfiles
    export(first.depends)
    export(copy_DeviceProfiles.commands)
    QMAKE_EXTRA_TARGETS += first copy_DeviceProfiles

TEMPLATE = app

SOURCES += main.cpp \
    mainWindow.cpp \
    workspaceEditor.cpp \
    ../../Trodes/src-config/configuration.cpp

HEADERS += \
    mainWindow.h \
    workspaceEditor.h \
    ../../Trodes/src-config/configuration.h
