#include "mainWindow.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    qInstallMessageHandler(moduleMessageOutput);
    setModuleName("SimpleCommunicator");

    QApplication a(argc, argv);
    qRegisterMetaType<TrodesEvent>("TrodesEvent");
    qRegisterMetaType<TrodesEventMessage>("TrodesEventMessage");
    qRegisterMetaType<QVector<TrodesEvent> >("QVector<TrodesEvent>");
    qRegisterMetaType<uint32_t>("uint32_t");
    MainWindow w(a.arguments());


    w.show();
    return a.exec();
}
