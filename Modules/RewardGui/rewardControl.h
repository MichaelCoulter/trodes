#ifndef __REWARD_CONTROL_H__
#define __REWARD_CONTROL_H__

#include <inttypes.h>
#include "sharedinclude.h"
#include "configuration.h"
#include <trodesSocket.h>
#include <QtGui>
#include <QtCore>
#include <QSound>
#include <QString>
#include <QSpinBox>
#include <QRadioButton>
#include <QPushButton>
#include <QButtonGroup>
#include <QMainWindow>
#include <QListWidget>
#include <QLabel>
#include <QGridLayout>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>



#define MAX_WELLS       64
#define MAX_REST        90000
#define MAX_REWARD_BITS 32

extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;

/* Reward GUI */
class rewardControl : public QMainWindow {
    Q_OBJECT

public:
    rewardControl(QStringList arguments, QWidget *parent = 0);
    ~rewardControl();
    void        DIOInput(DIOBuffer *diobuf);

    qint8 moduleID;

    QString rewardConfigFile;

    void                writeRewardConfig(QString fileName);
    void                readRewardConfig(QString fileName);
    void        connectSlots(void);

    TrodesModuleNetwork   *moduleNet;

public slots:
    void        createTabs(void)
    {
        createLogicTab(nWells->value());
        createStatusTab(nWells->value());
        if (avoidButton->isChecked()) {
            createAvoidTab();
        }
    };

private slots:

    void        load()
    {
        loadFile();
    };
    void        save()
    {
        saveFile();
    };

    void        setNextReward()
    {
        rewardWell(getNextWell(), true);
    };
    void        setReward(int well)
    {
        rewardWell(well, true);
    };
    void        setFirstReward(int well)
    {
        int i;

        if (firstTrial->isChecked()) {
            next[well]->setChecked(true);
            /* get rid of any other checked wells */
            for (i = 0; i < nWells->value(); i++) {
                if (i != well) {
                    next[i]->setChecked(false);
                }
            }
        }
        setStatus();
    };


    void         enableOutputNextBits(bool enabled)
    {
        /* Output 0 Bits are only defined for sequences of wells */
        if (!enabled) {
            nOutputNextBits->setValue(0);
        }
        nOutputNextBits->setEnabled(enabled);
    }


    void loadSequence(void);
    void setSoundFileName(int bit);
    void setSoundFileName(void);
    void startSound(int bit, QString filename);
    void startSound(QString filename); //for specifying a sound connected to a specific output bit

    void readData();

protected slots:
    void resetRewardCounters(void);
    void setAirTimer(void);
    void warnOff()
    {
        TriggerOutput(outputBit[1][0]->value());
    };
    void airOn()
    {
        TriggerOutput(outputBitAir->value());
    };
    void enableAudioBit(void);
    void closeGUI(void);
    void updateSendButton();
    void sendScript();
    void                startDataClientStreaming(void);


signals:
    void closeNow();



protected:
    void        closeEvent(QCloseEvent* event);
    void                loadFile(void);
    void                saveFile(void);
    void                createLogicTab(int n);
    void                createStatusTab(int n);
    void                createAvoidTab(void);
    void                createListWidgetItems(int nwells);
    void                createRewardListWidget(QListWidget *lw);

    void                rewardWell(int n, bool reward);
    int                 getPrevWell(void);
    int                 getNextWell(void);
    void                setStatus(void);

    QUdpSocket *udpDataSocket;


    QSound              **sound;
    QSound              *soundBit;
    QString             *audioFile;
    QString audioOutputBitFile;

    QSound              *soundOutputBit;


    QVector<int>        rewardCounter;
    QVector<qint16>     rewardWellFunction;
    QVector<qint16>     errorWellFunction;

    int prevWell;
    bool prevRewarded;

    /* tab widgets */
    QTabWidget          *qtab;
    QString tablabel;
    int ntabs;

    /* first tab: number of wells */
    QPushButton *fileLoad;
    QPushButton *fileSave;
    QLabel      *nWellsLabel;
    QSpinBox    *nWells;
    QLabel      *nOutputBitsLabel;
    QSpinBox    *nOutputBits;
    QLabel      *nOutputNextBitsLabel;
    QSpinBox    *nOutputNextBits;
    QPushButton *createTabsButton;
    QPushButton *close;

    // TEST
    //QPushButton *startData;

    QRadioButton *ignoreWellsButton;
    QRadioButton *useSequenceButton;
    QRadioButton *avoidButton;
    QRadioButton *audioButton;
    QRadioButton *audioBitButton;
    QLabel       *audioOutputBitLabel;
    QSpinBox     *audioOutputBit;
    QPushButton  *audioBitSoundFile;
    QRadioButton *errorBitButton;

    /* second tab - logic */

    QLabel              **wellLabel;
    QLabel              *prevLabel;
    QListWidget         **prev;
    QLabel              *currLabel;
    QListWidget         **curr;
    QLabel              *ignoreLabel;
    QListWidget         **ignore;
    QLabel              *inputBitLabel;
    QSpinBox            **inputBit;
    QRadioButton        **triggerHigh;
    QLabel              **outputBitLabel;
    QSpinBox            ***outputBit;
    QLabel              **outputBitLengthLabel;
    QSpinBox            ***outputBitLength;
    QLabel              **outputBitPercentLabel;
    QSpinBox            ***outputBitPercent;
    QLabel              **outputBitDelayLabel;
    QSpinBox            ***outputBitDelay;
    QLabel              *errorOutputBitLabel;
    QSpinBox            **errorOutputBit;
    QLabel              *errorOutputBitDelayLabel;
    QSpinBox            **errorOutputBitDelay;
    QLabel              *errorOutputBitLengthLabel;
    QSpinBox            **errorOutputBitLength;
    QLabel              **outputNextBitLabel;
    QSpinBox            ***outputNextBit;
    QLabel              **outputNextBitLengthLabel;
    QSpinBox            ***outputNextBitLength;
    QLabel              **outputNextBitDelayLabel;
    QSpinBox            ***outputNextBitDelay;
    QLabel              *outputNextSoundFileLabel;
    QPushButton         **outputNextSoundFile;
    QButtonGroup        *outputNextSoundButtonGroup;
    QPushButton *sendScriptButton;

    /* third tab */
    QRadioButton        *firstTrial;
    QLabel              *firstRewardLabel;
    QSpinBox            *firstRewardWell;
    QButtonGroup        *rewardButtonGroup;
    QPushButton         **reward;
    QRadioButton        **next;
    QLabel              **status;
    QPushButton         *nextReward;

    QPushButton         *nextWellOnErrorButton;
    QPushButton         *loadSequenceButton;
    QListWidget         *wellSequenceListWidget;

    /* fourth tab */
    QLabel              *restLengthLabel;
    QSpinBox            *restLength;
    QLabel              *warnLengthLabel;
    QSpinBox            *warnLength;
    QLabel              *warnPulseLabel;
    QSpinBox            *warnPulse;
    QLabel              *outputBitAirLabel;
    QSpinBox            *outputBitAir;
    QTimer              *timerRest;
    QTimer              *timerWarn;
    QTimer              *timerWarnOff;

private:
    /* define the list of bits that are on until the next trial */
    int nTrialBits;
    int trialBit[MAX_BITS];

    /* set a variable to indicate when we've gotten a close message from trodes */
    bool readyToClose;
};

/*class setRewardsDialog : public QDialog {
        Q_OBJECT

    public:
            setRewardsDialog(QWidget *parent = 0,
                    const char *name = 0, bool model = FALSE,
                    Qt::WFlags fl = 0, int port = 0);
            ~setRewardsDialog();
            int	port;
    signals:
            void finished(void);

    public slots:
        void	changePulseLength(int bit, unsigned short len) {
                        digioinfo.length[bit] = len; };
        void	changePulseDelay(int bit, u32 delay) {
                        digioinfo.delay[bit] = delay; };
        void	pulseBit(int bit) { TriggerOutput(bit + port * 16); };
        void	changeBit(int bit) {
                       if (digioinfo.raised[bit+port*16]) {
                            ChangeOutput(bit + port * 16, 0);
                       }
                       else {
                            ChangeOutput(bit + port * 16, 1);
                       }
                };
        void    gotoNextPort(void) { nextPort(); };
        void    gotoPrevPort(void) { prevPort(); };
        void    dialogClosed(void);

   protected slots:
    void reject();

    protected:
        void		nextPort();
        void		prevPort();
        QLabel		**outputNumberLabel;
        QLabel		**outputLengthLabel;
        SpikeLineEdit   **outputLength;
        QPushButton     **pulse;
        QRadioButton    **change;
        QButtonGroup	*pulseButtonGroup;
        QButtonGroup	*changeButtonGroup;
        QPushButton	*next;
        QPushButton	*prev;
        QPushButton	*close;

   };

   class fsDataDialog : public QDialog {
        Q_OBJECT

    public:
            fsDataDialog(QWidget *parent = 0,
                    const char *name = 0, bool model = FALSE,
                    Qt::WFlags fl = 0);
            ~fsDataDialog();
            void setFSDataInfo();

    public slots:
            void acceptSettings(void) { setFSDataInfo(); };

    signals:
            void finished(void);

    protected:
        QRadioButton    *posSelect;
        QRadioButton    *digioSelect;
        QLabel		*spikeLabel;
        QLabel		*contLabel;
        QRadioButton    **tetSpikeSelect;
        QRadioButton    **tetContSelect;
        QPushButton	*accept;
   };

 */
#endif // rewardControl.h
