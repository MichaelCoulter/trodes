#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>

class OldMainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit OldMainWindow(QStringList arguments, QWidget *parent = 0);
  ~OldMainWindow();

  QLabel *clock;

public slots:
  void changeStatusBarMessage(QString);
  void changeTimestampLabel(quint32 time);

signals:

private:
};

#endif // MAINWINDOW_H
