#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QMessageBox>
#include "configuration.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QStringList arguments, QWidget *parent = 0);
    ~MainWindow();

public slots:
    void changeStatusBarMessage(QString);
    void changeTimestamp(quint8 dataType, quint32 time);
    void changeContinuousMessage(QString msg);


private:
    Ui::MainWindow *ui;
    void changeTimestampLabel(QLabel *widget, quint32 time);

    QMenu* menuHelp;
    QAction* actionAbout;

private slots:
    void about();
};

#endif // MAINWINDOW_H
