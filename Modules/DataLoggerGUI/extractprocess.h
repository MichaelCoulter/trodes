#ifndef EXTRACTWINDOW_H
#define EXTRACTWINDOW_H

#include "abstractprocess.h"

class ExtractProcess : public AbstractProcess
{
    Q_OBJECT
public:
    explicit ExtractProcess(QString device, QString outputpath, QWidget *parent = 0);
    void start();
signals:
public slots:
private:
    QString device;
    QString outputpath;
//    QProcess *extractprocess;
//    QTextEdit *console;
    void customReadOutput(const QString &line);
//    void readOutput();
    void win_start_extract();
    void lin_start_extract();
    void mac_start_extract();
    void docking_extract();
};

#endif // EXTRACTWINDOW_H
