#-------------------------------------------------
#
# Project created by QtCreator 2018-04-06T15:13:24
#
#-------------------------------------------------


include(../module_defaults.pri)

QT       += core gui xml network
CONFIG += c++14
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DataLoggerGUI
TEMPLATE = app

CONFIG += console

INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-config
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-display
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-main
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-threads
INCLUDEPATH += $$TRODES_REPO_DIR/Modules/workspaceGUI

SOURCES += \
    controllerrfchannelprocess.cpp \
    controllersamplingrateprocess.cpp \
    dockconfigwidget.cpp \
    dockprogrammingprocess.cpp \
    jtagprocess.cpp \
        main.cpp \
        mainwindow.cpp \
    disklistwidget.cpp \
    extractprocess.cpp \
    mergeprocess.cpp \
    cardenableprocess.cpp \
    pcheckprocess.cpp \
    abstractprocess.cpp \
    sdtorecprocess.cpp \
    writeconfigprocess.cpp \
    readconfigprocess.cpp \
    ../workspaceGUI/workspaceEditor.cpp \
    ../../Trodes/src-display/quicksetup.cpp \
    ../../Trodes/src-display/workspaceeditordialog.cpp \
    ../../Trodes/src-display/cargrouppanel.cpp \
    ../../Trodes/src-config/configuration.cpp \
    dockingquickstart.cpp \
    ntrodechannelmappingwindow.cpp \
    loggerconfigwidget.cpp \
    firmwareupdater.cpp

HEADERS += \
    controllerrfchannelprocess.h \
    controllersamplingrateprocess.h \
    dockconfigwidget.h \
    dockprogrammingprocess.h \
    jtagprocess.h \
        mainwindow.h \
    disklistwidget.h \
    extractprocess.h \
    mergeprocess.h \
    cardenableprocess.h \
    pcheckprocess.h \
    abstractprocess.h \
    sdtorecprocess.h \
    writeconfigprocess.h \
    readconfigprocess.h \
    ../workspaceGUI/workspaceEditor.h \
    ../../Trodes/src-display/quicksetup.h \
    ../../Trodes/src-display/workspaceeditordialog.h \
    ../../Trodes/src-display/cargrouppanel.h \
    ../../Trodes/src-config/hardwaresettings.h \
    ../../Trodes/src-config/configuration.h \
    dockingquickstart.h \
    ntrodechannelmappingwindow.h \
    loggerconfigwidget.h \
    firmwareupdater.h

win32{
    INCLUDEPATH += $$PWD/../../Trodes/Libraries/Windows
    HEADERS += ../../Trodes/Libraries/Windows/ftd2xx.h
    LIBS += -L$${PWD}/../../Trodes/Libraries/Windows -lftd2xx

    RC_ICONS = loggerGUIIcon.ico
    RC_FILE = DataLoggerGUI.rc
    utilities.path = $$INSTALL_DIR/windows_sd_util/
    utilities.files += ./windows_sd_util/*

    mcu_utils.path = $$INSTALL_DIR/mcu_utils/windows
    mcu_utils.files += ./mcu_utils/windows/*

    INSTALLS += utilities mcu_utils

}

unix:!macx{
    INCLUDEPATH += ../../Trodes/Libraries/Linux
    LIBS += -L../../Trodes/Libraries/Linux
    HEADERS    += ../../Trodes/Libraries/Linux/WinTypes.h \
                  ../../Trodes/Libraries/Linux/ftd2xx.h
    LIBS       += -lftd2xx

    utilities.path = $$INSTALL_DIR/linux_sd_util/
    utilities.files += ./linux_sd_util/*

    icon.path = $$INSTALL_DIR/Resources/Images
    icon.files += ./dlGUIIcon.png

    mcu_utils.path = $$INSTALL_DIR/mcu_utils/linux
    mcu_utils.files += ./mcu_utils/linux/*

    INSTALLS += utilities mcu_utils icon

}
macx{
    utilities.path = $$INSTALL_DIR/macos_sd_util/
    utilities.files += ./macos_sd_util/*

    icon.path = $$INSTALL_DIR/Resources/Images
    icon.files += ./dlGUIIcon.png

    mcu_utils.path = $$INSTALL_DIR/mcu_utils/macos
    mcu_utils.files += ./mcu_utils/macos/*

    INSTALLS += utilities mcu_utils icon
}
RESOURCES += \
    ../../Resources/Images/buttons.qrc

workspaces.path = $$INSTALL_DIR/Resources/SampleWorkspaces
workspaces.files += $$TRODES_REPO_DIR/Resources/SampleWorkspaces/DockingStation.trodesconf
workspaces.files += $$TRODES_REPO_DIR/Resources/SampleWorkspaces/BlankWorkspace.trodesconf
INSTALLS += workspaces

DISTFILES += \
    DataloggerGUI

