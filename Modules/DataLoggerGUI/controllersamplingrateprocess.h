#ifndef CONTROLLERSAMPLINGRATEPROCESS_H
#define CONTROLLERSAMPLINGRATEPROCESS_H

#include "abstractprocess.h"

class ControllerSamplingRateProcess: public AbstractProcess
{
    Q_OBJECT
public:
    ControllerSamplingRateProcess(QWidget *parent=nullptr);//get
    ControllerSamplingRateProcess(int khzrate, QWidget *parent=nullptr);//get and set
    void start();

    bool mcu_success;
    bool dock_success;
private:
    bool setting_khzrate;
    int input_khzrate;
    void mcu_start_khzrate();
    void dock_start_khzrate();
protected:
    void customReadOutput(const QString &line);
};

#endif // CONTROLLERSAMPLINGRATEPROCESS_H
