#include "jtagprocess.h"

JTAGProcess::JTAGProcess(QString firmware, QWidget *parent)
    : AbstractProcess ("JTAGProcess", parent), firmwarefile(firmware)
{
}

void JTAGProcess::start(){
#if defined(Q_OS_WIN)
    win_start_jtag();
#elif defined(Q_OS_MAC)
    mac_start_jtag();
#elif defined(Q_OS_LINUX)
    lin_start_jtag();
#else
#error "OS not supported!"
#endif
}

void JTAGProcess::win_start_jtag(){
    process->start(".\\windows_sd_util\\mbftdi.exe", {firmwarefile, "DockingStation B"});
}

void JTAGProcess::lin_start_jtag(){
    process->start("./linux_sd_util/mbftdi", {firmwarefile, "DockingStation B"});
}

void JTAGProcess::mac_start_jtag()
{
    process->start("./macos_sd_util/mbftdi", {firmwarefile, "DockingStation B"});
}

void JTAGProcess::customReadOutput(const QString &line){
//    qDebug() << "---" << line;
}
