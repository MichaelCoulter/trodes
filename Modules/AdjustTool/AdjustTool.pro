#-------------------------------------------------
#
# Project created by QtCreator 2016-04-25T11:21:48
#
#-------------------------------------------------

QT       += core

include(../module_defaults.pri)

TARGET = AdjustTool
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../Source/src-config
INCLUDEPATH += ../../Source/src-main

SOURCES += \
    main.cpp \
    adjusttool.cpp

HEADERS += \
    adjusttool.h \
    ../../Source/src-config/configuration.h \
    ../../Source/src-main/trodesSocket.h \
    ../../Source/src-main/trodesSocketDefines.h \
