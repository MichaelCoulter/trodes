#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    AdjustTool adjustTool(a.arguments());

    return a.exec();
}
