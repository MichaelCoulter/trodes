include(../module_defaults.pri)

#CONFIG += debug
#CONFIG += console
CONFIG += warn_on
QT += opengl widgets xml multimedia multimediawidgets
UI_DIR = ./ui
MOC_DIR = ./moc
OBJECTS_DIR = ./obj

DEFINES += PHOTOMETRY_CODE

INCLUDEPATH += src-main
INCLUDEPATH += src-config
INCLUDEPATH += src-threads
INCLUDEPATH += src-display
INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
#stuff specific for mac
macx {
QMAKE_CXXFLAGS += -F/Library/Frameworks     #NIDAQmx Mac libraries
QMAKE_LFLAGS += -F/Library/Frameworks       #
LIBS += -framework nidaqmxbase              #
LIBS += -framework nidaqmxbaselv            #
HEADERS += "/Applications/National Instruments/NI-DAQmx Base/includes/NIDAQmxBase.h"

INCLUDEPATH += Libraries/Mac
ICON    = src-main/trodesIcon.icns
QMAKE_MAC_SDK = macosx10.12
}


unix:!macx {
INCLUDEPATH += Libraries/Linux
INCLUDEPATH += /usr/local/include  #assuming ftdi lib is in /usr/local/lib
}

win32 {
RC_ICONS += trodesIcon.ico
INCLUDEPATH += Libraries/Windows
HEADERS += "C:\Program Files (x86)\National Instruments\NI-DAQ\DAQmx ANSI C Dev\include\NIDAQmx.h"
LIBS += "C:\Program Files (x86)\National Instruments\NI-DAQ\DAQmx ANSI C Dev\lib\msvc\NIDAQmx.lib"
}


HEADERS   += \
    ../../Trodes/src-config/configuration.h \
    src-main/iirFilter.h \
    src-main/globalObjects.h\
    src-main/mainWindow.h\
    src-main/abstractTrodesSource.h \
    src-main/sourceController.h \
    src-display/streamDisplay.h \
    src-display/dialogs.h \
    ../../Trodes/src-main/trodesSocket.h \
    ../../Trodes/src-main/sharedVariables.h \
    ../../Trodes/src-main/trodesSocketDefines.h \
    ../../Trodes/src-main/trodesdatastructures.h \
    ../../Trodes/src-main/eventHandler.h \
    src-threads/streamProcessorThread.h \
    src-threads/fileSourceThread.h \
    src-threads/recordThread.h \
    src-threads/niusbdaqThread.h \
    src-threads/carrierThread.h \


SOURCES   += \
    ../../Trodes/src-config/configuration.cpp\
    src-main/iirFilter.cpp \
    src-main/main.cpp\
    src-main/mainWindow.cpp\
    src-main/abstractTrodesSource.cpp \
    src-main/sourceController.cpp \
    src-display/dialogs.cpp \
    src-display/streamDisplay.cpp \
    ../../Trodes/src-main/trodesSocket.cpp \
    ../../Trodes/src-main/trodesdatastructures.cpp \
    ../../Trodes/src-main/eventHandler.cpp \
    src-threads/fileSourceThread.cpp \
    src-threads/streamProcessorThread.cpp \
    src-threads/recordThread.cpp \
    src-threads/niusbdaqThread.cpp \
    src-threads/carrierThread.cpp






