/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "iirFilter.h"
#include <math.h>
#include <QtCore/qmath.h>
#include <QDebug>

BesselFilter::BesselFilter()
    :outputBufferSize(1024)
    ,outputBufferWritePos(0)
    ,dataWritten(0)
    ,isLowPass(false){

    //Coefficients were obtained from http://www-users.cs.york.ac.uk/~fisher/mkfilter
    //Bessel bandpass, designed with matched z-transform

    //Low pass filters are 2nd order, Band pass filters are 2nd order

    //25kHz sampling rate
    aCoefHashVect["25000_600_6000"] << -5.02815933e-02	 << 3.48684444e-01	<< -1.38919690e+00	<< 2.07810697e+00;
    gainHash["25000_600_6000"] = 1.36751963;

    aCoefHashVect["25000_500_6000"] << -4.75730559e-02	 << 3.38729900e-01	<< -1.40193286e+00	<< 2.10211093e+00;
    gainHash["25000_500_6000"] = 1.33245599;

    aCoefHashVect["25000_400_6000"] << -4.50104204e-02	 << 3.28850323e-01	<< -1.41513805e+00	<< 2.12584358e+00;
    gainHash["25000_400_6000"] = 1.29875076;

    aCoefHashVect["25000_300_6000"] << -4.25858273e-02	 << 3.19055281e-01	<< -1.42879574e+00	<< 2.14930825e+00;
    gainHash["25000_300_6000"] = 1.26628842;

    aCoefHashVect["25000_200_6000"] << -4.02918406e-02	 << 3.09353642e-01	<< -1.44288954e+00	<< 2.17250821e+00;
    gainHash["25000_200_6000"] = 1.23496107;

    aCoefHashVect["25000_600_5000"] << -8.74759847e-02	 << 5.69019625e-01	<< -1.74058481e+00	<< 2.24784666e+00;
    gainHash["25000_600_5000"] = 1.60543552;

    aCoefHashVect["25000_500_5000"] << -8.27638832e-02	 << 5.52744730e-01	<< -1.74282375e+00	<< 2.26521197e+00;
    gainHash["25000_500_5000"] = 1.55430544;

    aCoefHashVect["25000_400_5000"] << -7.83056102e-02	 << 5.36691196e-01	<< -1.74557249e+00	<< 2.28239258e+00;
    gainHash["25000_400_5000"] = 1.50567341;

    aCoefHashVect["25000_300_5000"] << -7.40874927e-02	 << 5.20865749e-01	<< -1.74881700e+00	<< 2.29939113e+00;
    gainHash["25000_300_5000"] = 1.45929276;

    aCoefHashVect["25000_200_5000"] << -7.00965940e-02	 << 5.05274487e-01	<< -1.75254342e+00	<< 2.31621017e+00;
    gainHash["25000_200_5000"] = 1.41493722;

    aCoefHashVect["25000_0_100"] << -9.46132627e-01	 << 1.94513845e+00;
    gainHash["25000_0_100"] = 1.00586139e+03;

    aCoefHashVect["25000_0_200"] << -8.95166947e-01	 << 1.89129835e+00;
    gainHash["25000_0_200"] = 2.58491728e+02;

    aCoefHashVect["25000_0_300"] << -8.46946655e-01	<< 1.83847820e+00;
    gainHash["25000_0_300"] = 1.18085246e+02;

    aCoefHashVect["25000_0_400"] << -8.01323864e-01	 << 1.78667556e+00;
    gainHash["25000_0_400"] = 6.82672909e+01;

    aCoefHashVect["25000_0_500"] << -7.58158652e-01	 << 1.73588713e+00;
    gainHash["25000_0_500"] = 4.49003913e+01;

    //30kHz sampling rate
    aCoefHashVect["30000_600_6000"] << -8.27638832e-02 << 5.52744730e-01 << -1.74282375e+00	 << 2.26521197e+00;
    gainHash["30000_600_6000"] = 1.55430544;

    aCoefHashVect["30000_500_6000"] << -7.90316183e-02	<<  5.39351119e-01	<< -1.74507956e+00	<<  2.27954186e+00;
    gainHash["30000_500_6000"] = 1.51361600;

    aCoefHashVect["30000_400_6000"] << -7.54676612e-02	<<  5.26115166e-01	<< -1.74768127e+00	<<  2.29374502e+00;
    gainHash["30000_400_6000"] = 1.47451718;

    aCoefHashVect["30000_300_6000"] << -7.20644219e-02	<< 5.13040488e-01	<< -1.75062083e+00	<<  2.30782293e+00;
    gainHash["30000_300_6000"] = 1.43687527;

    aCoefHashVect["30000_200_6000"] << -6.88146528e-02	<< 5.00130406e-01	<< -1.75389027e+00	<<  2.32177706e+00;
    gainHash["30000_200_6000"] = 1.40056555;

    aCoefHashVect["30000_600_5000"] << -1.31292736e-01	 << 8.11261017e-01	<< -2.13366443e+00	<< 2.44719019e+00;
    gainHash["30000_600_5000"] = 1.89565447;

    aCoefHashVect["30000_500_5000"] << -1.25372047e-01	 << 7.91119608e-01	<< -2.12566535e+00	 << 2.45547481e+00;
    gainHash["30000_500_5000"] = 1.83293842;

    aCoefHashVect["30000_400_5000"] << -1.19718353e-01	 << 7.71300258e-01	<< -2.11808631e+00	 << 2.46370798e+00;
    gainHash["30000_400_5000"] = 1.77333585;

    aCoefHashVect["30000_300_5000"] << -1.14319614e-01	 << 7.51802234e-01	<< -2.11092035e+00	 << 2.47189069e+00;
    gainHash["30000_300_5000"] = 1.71654394;

    aCoefHashVect["30000_200_5000"] << -1.09164333e-01	 << 7.32624654e-01	<< -2.10416047e+00	 << 2.48002389e+00;
    gainHash["30000_200_5000"] = 1.66228545;

    aCoefHashVect["30000_0_100"] << -9.54904667e-01	<< 1.95421109e+00;
    gainHash["30000_0_100"] = 1.44179090e+03;

    aCoefHashVect["30000_0_200"] << -9.11842923e-01	<< 1.90913163e+0;
    gainHash["30000_0_200"] = 3.68827887e+02;

    aCoefHashVect["30000_0_300"] << -8.70723063e-01	 << 1.86476091e+00;
    gainHash["30000_0_300"] = 1.67724692e+02;

    aCoefHashVect["30000_0_400"] << -8.31457516e-01	 << 1.821097740e+00;
    gainHash["30000_0_400"] = 9.65271770e+01;

    aCoefHashVect["30000_0_500"] << -7.93962663e-01	 << 1.778140493e+00;
    gainHash["30000_0_500"] = 6.32024557e+01;

    aCoefHashVect["1000_0_100"] << -2.50495817e-01 << 9.22123218e-01;
    gainHash["1000_0_300"] = 3.04532109e+00;

    aCoefHashVect["1000_0_200"] << -6.27481541e-02 << 3.49319595e-01;
    gainHash["1000_0_300"] = 1.40168204e+00;

    aCoefHashVect["1000_0_300"] << -1.57181501e-02 << 9.11277009e-02;
    gainHash["1000_0_300"] = 1.081559950e+00;


    //inputFreqHz = hardwareConf->sourceSamplingRate;
    outputBuffer = new int16_t[outputBufferSize];
    setSamplingRate(30000);
    setFilterRange(600,6000);

    //Initialize history to 0
    for (int i=0;i<3;i++) {
        xv[i] = 0;
    }
    for (int i=0;i<5;i++) {
        yv[i] = 0;
    }

}

BesselFilter::~BesselFilter() {
    delete outputBuffer;
}

void BesselFilter::setSamplingRate(int rate) {
    inputFreqHz = rate;
}

void BesselFilter::resetHistory() {
    //Initialize history to 0
    for (int i=0;i<3;i++) {
        xv[i] = 0;
    }
    for (int i=0;i<5;i++) {
        yv[i] = 0;
    }
}

int16_t BesselFilter::addValue(int16_t value) {
    //This function takes in a new raw value, adds it to the filter history,
    //and returns the filtered value.

    if (isLowPass) {
        //2nd order lowpass has two poles
        xv[0] = (double) value / gain;
                yv[0] = yv[1]; yv[1] = yv[2];
                yv[2] =   bcoef[0]*xv[0]
                             + ( acoef[0] * yv[0]) + (  acoef[1] * yv[1]);
                actualOutputValue = (int16_t) yv[2];

    } else {
        //2nd order bandpass has 4 poles
        xv[0] = xv[1]; xv[1] = xv[2];
        xv[2] = (double) value / gain;
        yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4];
        yv[4] =   (bcoef[0]*xv[0] + bcoef[1]*xv[1] + bcoef[2]*xv[2])
                         + ( acoef[0] * yv[0]) + (  acoef[1] * yv[1])
                         + ( acoef[2] * yv[2]) + (  acoef[3] * yv[3]);
        actualOutputValue = (int16_t) yv[4];
    }

    outputBuffer[outputBufferWritePos] = actualOutputValue;
    outputBufferWritePos = (outputBufferWritePos+1) % outputBufferSize;
    dataWritten++;

    return actualOutputValue;

}

void BesselFilter::setFilterRange(int lower, int upper) {
    upperCutoffHz = upper;
    lowerCutoffHz = lower;
    calcCoef();
}

void BesselFilter::calcCoef() {


    if (lowerCutoffHz ==0) {
        isLowPass = true;
    } else {
        isLowPass = false;
    }
    //Set the coefficients based on the hash key
    QString filterSettingString;

    filterSettingString.append(QString("%1_").arg(inputFreqHz));
    filterSettingString.append(QString("%1_").arg(lowerCutoffHz));
    filterSettingString.append(QString("%1").arg(upperCutoffHz));

    if (!aCoefHashVect.contains(filterSettingString)) {
        qDebug() << "ERROR: BesselFilter does not have filters for " << filterSettingString;
    }
    for (int i=0; i < aCoefHashVect.value(filterSettingString).length(); i++) {

        acoef[i] = aCoefHashVect.value(filterSettingString).value(i);
    }

    /*
    acoef[0] = aCoefHashVect.value(filterSettingString).value(0);
    acoef[1] = aCoefHashVect.value(filterSettingString).value(1);
    acoef[2] = aCoefHashVect.value(filterSettingString).value(2);
    acoef[3] = aCoefHashVect.value(filterSettingString).value(3);
    */

    gain = gainHash.value(filterSettingString);

    //qDebug() << "New filter values: " << acoef[0] << acoef[1] << acoef[2] << acoef[3];


    bcoef[0] = 1.0;
    bcoef[1] = -2.0;
    bcoef[2] = 1.0;


}


ButterworthFilter::ButterworthFilter()
    : currentValue(0)
    ,outputBufferSize(1024)
    ,outputBufferWritePos(0)
    ,dataWritten(0){

    denominatorHash["30000_211_0"][0] = -6.80140642165588	;
    denominatorHash["30000_211_0"][1] = 19.8280890383825	;
    denominatorHash["30000_211_0"][2] = -32.1181032752607	;
    denominatorHash["30000_211_0"][3] = 31.2197055438216	;
    denominatorHash["30000_211_0"][4] = -18.2102894040490	;
    denominatorHash["30000_211_0"][5] = 5.90186864707985	;
    denominatorHash["30000_211_0"][6] = -0.819864128020052;
    denominatorHash["30000_211_0"][7] = 0	;
    denominatorHash["30000_211_0"][8] = 0	;
    denominatorHash["30000_211_0"][9] = 0	;
    denominatorHash["30000_211_0"][10] = 0  ;

    numeratorHash["30000_211_0"][0] = 0.905463487955231	;
    numeratorHash["30000_211_0"][1] = -6.33824441568661	;
    numeratorHash["30000_211_0"][2] = 19.0147332470598	;
    numeratorHash["30000_211_0"][3] = -31.6912220784331	;
    numeratorHash["30000_211_0"][4] = 31.6912220784331	;
    numeratorHash["30000_211_0"][5] = -19.0147332470598	;
    numeratorHash["30000_211_0"][6] = 6.33824441568661	;
    numeratorHash["30000_211_0"][7] = -0.905463487955231;
    numeratorHash["30000_211_0"][8] = 0;
    numeratorHash["30000_211_0"][9] = 0;
    numeratorHash["30000_211_0"][10] = 0;
    numeratorHash["30000_211_0"][11] = 0	;

    denominatorHash["30000_531_0"][0] = -6.50025240208513	;
    denominatorHash["30000_531_0"][1] = 18.1252864877337	;
    denominatorHash["30000_531_0"][2] = -28.1031234377934	;
    denominatorHash["30000_531_0"][3] = 26.1667842820523	;
    denominatorHash["30000_531_0"][4] = -14.6305357385173	;
    denominatorHash["30000_531_0"][5] = 4.54830159605463	;
    denominatorHash["30000_531_0"][6] = -0.606460622397370;
    denominatorHash["30000_531_0"][7] = 0	;
    denominatorHash["30000_531_0"][8] = 0	;
    denominatorHash["30000_531_0"][9] = 0	;
    denominatorHash["30000_531_0"][10] = 0  ;

    numeratorHash["30000_531_0"][0] = 0.778755816926827	;
    numeratorHash["30000_531_0"][1] = -5.45129071848779	;
    numeratorHash["30000_531_0"][2] = 16.3538721554634	;
    numeratorHash["30000_531_0"][3] = -27.2564535924390	;
    numeratorHash["30000_531_0"][4] = 27.2564535924390	;
    numeratorHash["30000_531_0"][5] = -16.3538721554634	;
    numeratorHash["30000_531_0"][6] = 5.45129071848779	;
    numeratorHash["30000_531_0"][7] = -0.778755816926827;
    numeratorHash["30000_531_0"][8] = 0;
    numeratorHash["30000_531_0"][9] = 0;
    numeratorHash["30000_531_0"][10] = 0;
    numeratorHash["30000_531_0"][11] = 0	;

    denominatorHash["30000_0_15"][0] = -2.99371681727665;
    denominatorHash["30000_0_15"][1] = 2.98745335824285	;
    denominatorHash["30000_0_15"][2] = -0.993736510057099;
    denominatorHash["30000_0_15"][3] = 0	;
    denominatorHash["30000_0_15"][4] = 0	;
    denominatorHash["30000_0_15"][5] = 0	;
    denominatorHash["30000_0_15"][6] = 0	;
    denominatorHash["30000_0_15"][7] = 0	;
    denominatorHash["30000_0_15"][8] = 0	;
    denominatorHash["30000_0_15"][9] = 0	;
    denominatorHash["30000_0_15"][10] = 0   ;

    numeratorHash["30000_0_15"][0] = 3.86363707693960e-09	;
    numeratorHash["30000_0_15"][1] = 1.15909112308188e-08	;
    numeratorHash["30000_0_15"][2] = 1.15909112308188e-08	;
    numeratorHash["30000_0_15"][3] = 3.86363707693960e-09	;
    numeratorHash["30000_0_15"][4] = 0	;
    numeratorHash["30000_0_15"][5] = 0	;
    numeratorHash["30000_0_15"][6] = 0	;
    numeratorHash["30000_0_15"][7] = 0	;
    numeratorHash["30000_0_15"][8] = 0	;
    numeratorHash["30000_0_15"][9] = 0	;
    numeratorHash["30000_0_15"][10] = 0	;
    numeratorHash["30000_0_15"][11] = 0 ;

    for (int n = 0; n < 12; n++) {
        hpfilterInput_section[n] = 0;
        hpfilterOutput_section[n] = 0;
        lpfilterInput_section[n] = 0;
        lpfilterOutput_section[n] = 0;
    }
    outputBuffer = new double[outputBufferSize];

}

ButterworthFilter::~ButterworthFilter() {
    delete outputBuffer;
}

void ButterworthFilter::setSamplingRate(int rate) {
    inputFreqHz = rate;
}

void ButterworthFilter::resetHistory() {
    for (int n = 0; n < 12; n++) {
        hpfilterInput_section[n] = 0;
        hpfilterOutput_section[n] = 0;
        lpfilterInput_section[n] = 0;
        lpfilterOutput_section[n] = 0;
    }
}

double ButterworthFilter::addValue_lp(double value) {

    for (int n = 11; n > 0; n--) {
        lpfilterInput_section[n] = lpfilterInput_section[n-1];
        lpfilterOutput_section[n] = lpfilterOutput_section[n-1];
    }

    lpfilterInput_section[0] = value;
    lpfilterOutput_section[0] = bcof_lowpass[0] * lpfilterInput_section[0];

    for (int n = 1; n < 12; n++) {
        lpfilterOutput_section[0] += (bcof_lowpass[n] * lpfilterInput_section[n]) - (acof_lowpass[n-1] * lpfilterOutput_section[n]);
    }

    actualOutputValue = lpfilterOutput_section[0];

    return actualOutputValue;

}

double ButterworthFilter::addValue_hp(double value) {

    for (int n = 11; n > 0; n--) {
        hpfilterInput_section[n] = hpfilterInput_section[n-1];
        hpfilterOutput_section[n] = hpfilterOutput_section[n-1];
    }

    hpfilterInput_section[0] = value;
    hpfilterOutput_section[0] = bcof_highpass[0] * hpfilterInput_section[0];

    for (int n = 1; n < 12; n++) {
        hpfilterOutput_section[0] += (bcof_highpass[n]   * hpfilterInput_section[n])-
                                     (acof_highpass[n-1] * hpfilterOutput_section[n]);
    }

    actualOutputValue = hpfilterOutput_section[0];

    return actualOutputValue;

}

void ButterworthFilter::setFilterRange(int lower, int upper ) {
    upperCutoffHz = upper;
    lowerCutoffHz = lower;
    calcCoef();
}

void ButterworthFilter::calcCoef() {

    //Set the coefficients based on the hash key
    QString filterSettingString;
    filterSettingString.append(QString("%1_").arg(inputFreqHz));
    filterSettingString.append(QString("%1_").arg(0));
    filterSettingString.append(QString("%1").arg(lowerCutoffHz));

    for (int i = 0; i< 11 ; i++){
        acof_lowpass[i] = denominatorHash.value(filterSettingString).value(i);
    }

    for (int i = 0; i< 12 ; i++){
        bcof_lowpass[i] = numeratorHash.value(filterSettingString).value(i);
    }

    filterSettingString.clear();
    filterSettingString.append(QString("%1_").arg(inputFreqHz));
    filterSettingString.append(QString("%1_").arg(upperCutoffHz));
    filterSettingString.append(QString("%1").arg(0));

    for (int i = 0; i< 11 ; i++){
        acof_highpass[i] = denominatorHash.value(filterSettingString).value(i);
    }

    for (int i = 0; i< 12 ; i++){
        bcof_highpass[i] = numeratorHash.value(filterSettingString).value(i);
    }

}
