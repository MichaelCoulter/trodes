/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UI_MAIN_H
#define UI_MAIN_H

#include <unistd.h>
#include <QtGui>
#include <QAudioOutput>
#include <QAudioFormat>
#include "streamProcessorThread.h"
#include "recordThread.h"
#include "trodesSocket.h"
#include "streamDisplay.h"
#include "dialogs.h"
#include "globalObjects.h"
#include "sourceController.h"

#define TIMESLIDERSTEPS 50000

QT_BEGIN_NAMESPACE

class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

class MySlider : public QSlider {

Q_OBJECT

public:
    MySlider(QWidget *parent = 0);

private:
    int startBorder;
    int endBorder;
    bool draggingStart;
    bool draggingEnd;
    bool draggingKnob;
    bool logMarker[TIMESLIDERSTEPS];

public slots:
    void setStart(int newStartBorder); //set the start of the time range
    void setEnd(int newEndBorder); //set the end of the time range
    void markCurrentLocation();
    void clearLogMarks();
    void clearLogMarksAfterCurrentPos();


protected:
    void mousePressEvent (QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

signals:

    void newRange(int start,int end);
};

class MainWindow : public QMainWindow
{
  Q_OBJECT

private:
    int                         findEndOfConfigSection(QString filename);

    QTabWidget                  *tabs;
    QGridLayout                 *mainLayout;
    QGridLayout                 *headerLayout;
    QLabel                      *timeLabel;
    QLabel                      *fileLabel;
    TrodesButton                 *videoButton;
    TrodesButton                 *recordButton;
    TrodesButton                 *pauseButton;
    TrodesButton                 *playButton;
    QString                     fileString;
    bool                        soundDialogOpen;
    bool                        recordFileOpen;
    QString                     recordFileName;
    bool                        dataStreaming;
    bool                        moduleDataStreaming;
    bool                        recording;
    int                         timerTick;
    bool                        eventTabWasChanged;
    QString                     calcTimeString();
    QString                     currentConfigFileName;
    QString                     loadedConfigFile;
    int64_t                     visibleTime;
    bool                        eventTabsInitialized[64];
    int                         currentTrodeSelected;
    bool                        singleTriggerWindowOpen;
    bool                        channelsConfigured;
    QTimer                      *pullTimer;
    bool                        quitting;  //True if the program is in the process of quitting

    StreamProcessorManager      *streamManager;
    SourceController            *sourceControl;
    StreamDisplayManager        *eegDisp;
    RecordThread                *recordOut;

    TrodesModuleNetwork         *moduleNet;
    unsigned char               currentOperationMode;


    //module
    bool fileInitiated;
    QString serverAddress;
    QString serverPortValue;
    QString trodesConfigFile;
    int moduleInstance;
    QString fileName;
    uint32_t clockRate;
    bool inputFileOpen;
    bool playbackMode;
    bool loggingOn;
    bool justSeekedFlag;
    bool fileOpen;
    QString baseFileName;
    QString inputFileName;
    quint32 fileStartTime;
    quint32 fileEndTime;
    quint32 startSliderTime;
    quint32 endSliderTime;
    bool videoWasPlayingBeforeSliderPress;
    bool isVideoFilePlaying;
    bool isVideoSliderPressed;
    MySlider                    *filePositionSlider;
    //

private slots:


protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void paintEvent(QPaintEvent *);


public:

    QMenu *menuFile;
    QMenu *menuConfig;
    QAction *actionLoadConfig;
    QAction *actionCloseConfig;
    QAction *actionCloseFile;
    QAction *actionRecord;
    QAction *actionPause;
    QAction *actionPlay;
    QAction *actionQuit;

    QMenu   *menuSystem;
    QMenu   *sourceMenu;

    QMenu   *menuSimulationSource;
    QMenu   *menuSpikeGadgetsSource;

    QAction *actionSourceNone;
    QAction *actionSourceFile;
    QAction *actionSourceEthernet;

    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionClearBuffers;

    QMenu   *menuHelp;
    QAction *actionAboutQT;

    QMenu   *menuDisplay;
    QMenu   *menuEEGDisplay;
    QMenu   *menuSetTLength;
    QAction *actionSetTLength0_2;
    QAction *actionSetTLength0_5;
    QAction *actionSetTLength1_0;
    QAction *actionSetTLength2_0;
    QAction *actionSetTLength5_0;
    QMenu   *streamFilterMenu;
    QAction *streamFilterLink;
    QAction *streamFilterUnLink;

    QStatusBar *statusbar;
    QWidget *centralwidget;

    MainWindow(QStringList arguments);
    ~MainWindow();



    void retranslateUi()
    {
        setWindowTitle(QApplication::translate("Main", "Photometry", 0));
        menuConfig->setTitle(QApplication::translate("Main", "Workspace", 0));
        actionLoadConfig->setText(QApplication::translate("Main", "Load...", 0));
        actionCloseConfig->setText(QApplication::translate("Main", "Close", 0));
        actionCloseFile->setText(QApplication::translate("Main", "Close file", 0));
        actionRecord->setText(QApplication::translate("Main", "Record", 0));
        actionPause->setText(QApplication::translate("Main", "Pause", 0));
        actionPlay->setText(QApplication::translate("Main", "Play file", 0));

        menuSimulationSource->setTitle(QApplication::translate("Main", "Simulation", 0));
        menuSpikeGadgetsSource->setTitle(QApplication::translate("Main", "SpikeGadgets", 0));

        actionSourceNone->setText(QApplication::translate("Main", "None", 0));
        actionSourceFile->setText(QApplication::translate("Main", "&File", 0));
        actionSourceEthernet->setText(QApplication::translate("Main", "Ethernet", 0));

        actionConnect->setText(QApplication::translate("Main", "Stream from source", 0));
        actionDisconnect->setText(QApplication::translate("Main", "Disconnect", 0));
        actionClearBuffers->setText(QApplication::translate("Main", "Clear buffers", 0));
        actionQuit->setText(QApplication::translate("Main", "Exit", 0));
        actionAboutQT->setText(QApplication::translate("Main", "About QT", 0));
        sourceMenu->setTitle(QApplication::translate("Main", "Source", 0));
        menuHelp->setTitle(QApplication::translate("Main", "Help", 0));
        menuEEGDisplay->setTitle(QApplication::translate("Main", "Streaming", 0));
        menuSetTLength->setTitle(QApplication::translate("Main", "Trace length", 0));
        actionSetTLength0_2->setText(QApplication::translate("Main", "0.2 Seconds", 0));
        actionSetTLength0_5->setText(QApplication::translate("Main", "0.5 Seconds", 0));
        actionSetTLength1_0->setText(QApplication::translate("Main", "1.0 Seconds", 0));
        actionSetTLength2_0->setText(QApplication::translate("Main", "2.0 Seconds", 0));
        actionSetTLength5_0->setText(QApplication::translate("Main", "5.0 Seconds", 0));
        streamFilterMenu->setTitle(QApplication::translate("Main", "Filters", 0));
        streamFilterLink->setText(QApplication::translate("Main", "Link to spike filters", 0));
        streamFilterUnLink->setText(QApplication::translate("Main", "No filters", 0));

    } // retranslateUi

signals:
    void configFileLoaded();
    void closeAllWindows();
    void closeSoundDialog();
    void closeWaveformDialog();
    void endAllThreads();
    void endAudioThread();
    void newTraceLength(double);
    void newTimeStamp();

    void sourceConnected(QString source);
    void recordFileClosed();

    void recordingStarted();
    void recordingStopped();

    void messageForModules(TrodesMessage *tm);


public slots:

    void selectTrode(int nTrode);
    void openPlaybackFile(const QString fileName);
    void eventTabChanged(int);
    void setSourceMenuState(int);
    void updateTime();
    void linkFilters();
    void unLinkFilters();
    void linkChanges();
    void unLinkChanges();

    void setAllMaxDisp(int newMaxDisp);
    void setTLength();
    void setSource();
    void setSource(DataSource source);
    void connectToSource();
    void disconnectFromSource();
    void loadConfig();
    int loadConfig(QString);
    void closeConfig();
    void openRecordDialog(QString filename);
    void closeFile();
    void recordButtonPressed();
    void pauseButtonPressed();
    void playButtonPressed();
    void recordButtonReleased();
    void pauseButtonReleased();
    void playButtonReleased();
    void actionRecordSelected();
    void actionPauseSelected();
    void actionPlaySelected();
    void removeFromOpenNtrodeList(int nTrodeNum);
    void bufferOverrunHandler();
    void showErrorMessage(QString msg);
    void errorSaving();
    void restartCrashedModule(QString modName);

    void threadError(QString errorString);

    //Module
    void setPhotometrySource(QString source);
    void setTime(quint32 t);
    void setTimeRate(quint32 inTimeRate) ;
    void startRecording() ;
    void stopRecording() ;
    void closePFile();
    void setModuleInstance(int instanceNum);
    void setFileClosedStatus();

};

QT_END_NAMESPACE

#endif // MAIN_H
