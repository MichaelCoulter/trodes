
For linux machines, "readConfig_linux" will read and translate a config file from the SD card
(usually /dev/sdXXX). "check_recorded_data" will look for dropped packets, and
"extract_recorded_data" will copy data (minus header) to a file. "extract_recorded_data"
probably should have input arguments that parse the file intelligently (i.e., digital IO, by
channel, etc).
