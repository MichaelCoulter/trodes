#include "trodesdatastructures.h"


template <class T>
void avgCalculator<T>::insert(T obj) {
    data.append(obj);
    sum += obj;
    movAvgData.append(obj);
    movAvgSum += obj;
    if (movAvgData.length() > movAvgMaxSetSize) { //keep moving average set within the size we've specified
        T objToRemove = movAvgData.first();
        movAvgData.removeFirst();
        movAvgSum -= objToRemove;
    }
    if (obj < min || min == -1) {
        min = obj;
    }
    if (obj > max) {
        max = obj;
    }
}

//define all known types here that will be used in the future
template class avgCalculator<int>;
template class avgCalculator<float>;
template class avgCalculator<qreal>;

//dataSend() *******************************
dataSend::dataSend() {
    DataType t = DT_NULL;
    QVariant v = -1;
    int b = 0;
    init(t,v,b);
}

dataSend::dataSend(const dataSend &obj) {
    init(obj.getType(),obj.getObj(),obj.getSize());
}

/**
template <typename T>
dataSend::dataSend(DataType ty, T cont) {
    init(ty,QVariant(cont),sizeof(T));
}**/

int dataSend::sizeOfType(DataType ty) {
    int retval = 0;
    switch(ty) {
    case DT_int: {
        retval = sizeof(int);
        break;
    }
    case DT_qreal: {
        retval = sizeof(qreal);
        break;
    }
    case DT_uint8_t: {
        retval = sizeof(uint8_t);
        break;
    }
    case DT_uint32_t: {
        retval = sizeof(uint32_t);
        break;
    }
    case DT_int16_t: {
        retval = sizeof(int16_t);
        break;
    }
    default: {
        qDebug() << "WARNING: Bad type size requested. (dataSend::sizeOfType)";
        break;
    }
    }
    return(retval);
}

void dataSend::printAtr(void) {
    qDebug() << "dataSend Attributes:";
    qDebug() << "-Type: " << type;
    qDebug() << "-Container: " << container;
    qDebug() << "-Bytes: " << bytes;
}


void dataSend::init(DataType ty, QVariant cont, int by) {
    type = ty;
    container = cont;
    bytes = by;
}

//dataPacket() *******************************

dataPacket::dataPacket() {
    type = PPT_NULL;
    totalBytes = 0;
}

dataPacket::dataPacket(PPacketType ty) {
    type = ty;
    totalBytes = 0;
}

dataPacket::dataPacket(const dataPacket &obj)  {
    type = obj.type;
    totalBytes = obj.totalBytes;
    for (int i = 0; i < obj.dataLength(); i++) {
        data.append(obj.data.at(i));
    }
}

dataPacket::~dataPacket() {

}

void dataPacket::insert(dataSend someData) {
    data.append(someData);
    totalBytes += someData.getSize();
    //qDebug() << " size: " << totalBytes;
}

dataSend dataPacket::getDataAt(int index) const {
    if (index < data.length() && index >= 0) {
        return(data.at(index));
    }
    else {
        dataSend error = dataSend();
        return(error);
    }

}

void dataPacket::printAtr() const {
    qDebug() << "DataPacket attributes:";
    qDebug() << "-type: " << type;
    qDebug() << "-totalBytes: " << totalBytes;
    qDebug() << "-Contains:";
    for (int i = 0; i < data.length(); i++) {
        qDebug() << " -Item[" << i << "]:";
        //int ty = data.at(i).getType();

        qDebug() << "   -Type: " << data.at(i).getType();
        qDebug() << "   -Container: " << data.at(i).getObj();
        qDebug() << "   -Bytes: " << data.at(i).getSize();
    }

}

TrodesEvent::TrodesEvent() {
    TrodesEvent("","",-1);
}

TrodesEvent::TrodesEvent(QString name, QString mod, qint8 modID) {
    eventName = name;
    parentModule = mod;
    parentModuleID = modID;
}

TrodesEvent::TrodesEvent(const TrodesEvent &obj) {
    eventName = obj.getEventName();
    parentModule = obj.getParentModule();
    parentModuleID = obj.getParentModuleID();
}

void TrodesEvent::printEventInfo(void) const {
    QString event = QString("Event: [%1-(%2)] [%3]").arg(parentModule).arg(parentModuleID).arg(eventName);
    qDebug() << event;
}

TrodesEventMessage::TrodesEventMessage() {
    TrodesEventMessage("");
}

TrodesEventMessage::TrodesEventMessage(QString eventMess) {
    eventMessage = eventMess;
    setEventTimeToCur();
}

