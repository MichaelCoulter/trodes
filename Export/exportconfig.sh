#!/bin/bash
# Extract the .trodesconf config file at the start of a .rec file. Accepts 
# one input, which must be a .rec file, and writes out a .trodesconf file 
# with the same basename, in the current directory.

if [[ "${1: -4}" != ".rec" ]]
  then 
    echo "Input is not a .rec file"
    exit 1
fi

bn=$(basename $1 .rec)
if [ -e "$bn.trodesconf" ]
  then 
    echo "$bn.trodesconf already exists, exiting"
    exit 1
  else 
    echo "Creating $bn.trodesconf"
    sed -n -e "w  $bn.trodesconf" -e "/<\/Configuration>/q" $1
fi
exit 0
