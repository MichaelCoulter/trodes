
#-------------------------------------------------
#
# Project created by QtCreator 2015-11-16T09:07:52
#
#-------------------------------------------------

include(../export_defaults.pri)

TARGET = exportanalog
INCLUDEPATH  += ../exportanalog

SOURCES += main.cpp \
           datexporthandler.cpp

HEADERS  += datexporthandler.h
