#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{
    supportedDataCategories = DataCategories::RawBand | DataCategories::ContinuousOutput;

    //Change defaults here
    maxGapSizeInterpolation = 0;

    parseArguments();


    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract the raw band (continuous data) from a raw rec file and save to individual files. By default, the data are referenced (or not) using the settings in the workspace. \n");
    printf("Usage:  exportraw -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n"
           "    -outputrate -1 \n"
           "    -interp 0 \n\n");

        //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    QString opn_string;
    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }
        optionInd++;
    }


    AbstractExportHandler::parseArguments();

}


int DATExportHandler::processData() {
    qDebug() << "Processing raw...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).

    calculateChannelInfo();
    //createFilters();


    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".raw"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating raw directory.";
            return -1;
        }
    }

    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    QString infoLine;
    timeFilePtrs.last()->write("<Start settings>\n");
    infoLine = QString("Description: Raw file timestamps\n");
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Decimation: %1\n").arg(decimation);
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());

    writeDefaultHeaderInfo(timeFilePtrs.last());



    QString fieldLine;
    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\n";
    timeFilePtrs.last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs.last()->write("<End settings>\n");
    timeFilePtrs.last()->flush();




    //Create an output file for the neural data
    //*****************************************




    //Create the output files (one for each channel) and write header sections
    for (int nt=0; nt < spikeConf->ntrodes.length(); nt++) {
        for (int chInd = 0; chInd < spikeConf->ntrodes[nt]->hw_chan.length(); chInd++) {
            bool saveThisChannel = true;

            if (saveThisChannel) {
                neuroFilePtrs.push_back(new QFile);
                neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".raw_nt%1ch%2.dat").arg(spikeConf->ntrodes[nt]->nTrodeId).arg(chInd+1));
                if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file.";
                    return -1;
                }
                neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
                neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

                //Write the current settings to file

                neuroFilePtrs.last()->write("<Start settings>\n");
                infoLine = QString("Description: RAW continuous data for one channel\n");
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Byte_order: little endian\n");
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Original_file: ") + fi.fileName() + "\n";
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("nTrode_ID: %1\n").arg(spikeConf->ntrodes[nt]->nTrodeId);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("nTrode_channel: %1\n").arg(spikeConf->ntrodes[nt]->moduleDataChan+1);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Clock rate: %1\n").arg(hardwareConf->sourceSamplingRate);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Voltage_scaling: %1\n").arg(0.195);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Decimation: %1\n").arg(decimation);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("System_time_at_creation: %1\n").arg(globalConf->systemTimeAtCreation);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Timestamp_at_creation: %1\n").arg(globalConf->timestampAtCreation);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("First_timestamp: %1\n").arg(currentTimeStamp);
                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

                if (spikeConf->ntrodes[nt]->rawRefOn) {
                    if (spikeConf->ntrodes[nt]->groupRefOn) {
                        infoLine = QString("Reference: Common average reference group %1\n").arg(spikeConf->ntrodes[nt]->refGroup);
                    } else {
                        infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\n").arg(spikeConf->ntrodes[nt]->refNTrodeID).arg(spikeConf->ntrodes[nt]->refChan+1);
                    }
                } else {
                    infoLine = QString("Reference: off\n");
                }

                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

                /*if (spikeConf->ntrodes[nt]->lfpFilterOn) {
                    infoLine = QString("Low_pass_filter: %1\n").arg(spikeConf->ntrodes[nt]->moduleDataHighFilter);

                } else {
                    infoLine = QString("Low_pass_filter: none\n");
                }*/


                neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
                writeDefaultHeaderInfo(neuroFilePtrs.last());

                fieldLine.clear();
                fieldLine += "Fields: ";
                //fieldLine += "<time uint32>";
                fieldLine += "<voltage int16>";
                fieldLine += "\n";
                neuroFilePtrs.last()->write(fieldLine.toLocal8Bit());


                neuroFilePtrs.last()->write("<End settings>\n");
                neuroFilePtrs.last()->flush();
            }
        }
    }


    //************************************************
    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }



        }


        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {


            if (!getNextPacket()) {
                //We have reached the end of the file
                break;
            }


            if ((currentTimeStamp%decimation) == 0) { //if we are subsampling the data, make sure this is a point we want to save in the first place
                //save data in time file

                *timeStreamPtrs.at(0) << currentTimeStamp; //Write the data to disk
                int fileInd = 0;
                for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
                    const int16_t* tmpNtrodeData;
                    if (spikeConf->ntrodes[ntInd]->rawRefOn) {
                        tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
                    } else {
                        tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_UNREFERENCED);
                    }


                    for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {

                            *neuroStreamPtrs.at(fileInd) << tmpNtrodeData[chInd]; //Write the data to disk
                            fileInd++; //This works because the original files were created with the exact same nested loops with the same check

                    }
                }
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStampInFile = currentTimeStamp;

        }

        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;

    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }



    return 0; //success
}
