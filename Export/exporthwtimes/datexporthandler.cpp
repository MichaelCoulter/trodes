#include "datexporthandler.h"

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{
    recFileName = "";
    dioFileName = "";
    dioChannel = "";
    videoTimeStampsFileName = "";
    hwSyncFileName = "";
    avgIncrease = 0.0;
    parseArguments();
}
DATExportHandler::~DATExportHandler(){

}

void DATExportHandler::parseArguments(){
    int optionInd = 1;

    QString hwSync_string = "";
    QString videostampsfile_string = "";
    QString diobinary_string = "";
    QString h264_string = "";



    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0) || (argumentList.at(optionInd).compare("--help",Qt::CaseInsensitive)==0)) {
            printHelpMenu();
        } else if((argumentList.at(optionInd).compare("-hwsync", Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)){
            hwSync_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed += 2;
        } else if ((argumentList.at(optionInd).compare("-vidstamps",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            videostampsfile_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed += 2;
        }  else if ((argumentList.at(optionInd).compare("-dio",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            diobinary_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed += 2;
        }  else if ((argumentList.at(optionInd).compare("-h264",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            h264_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed += 2;
        }
        optionInd++;
    }

    //CHECK VALID ARGUMENT STRINGS HERE
    hwSyncFileName = hwSync_string;
    dioFileName = diobinary_string;
    videoTimeStampsFileName = videostampsfile_string;
    h264FileName = h264_string;

    if(!h264FileName.isEmpty()){
        //Find video files automatically
        QFileInfo vfile(h264FileName);
        QDir dir = vfile.absoluteDir();
        QStringList filter(vfile.completeBaseName() + ".videoTimeStamps*");
        QFileInfoList files = dir.entryInfoList(filter);
        for(int i = 0; i < files.length(); i++){
            if(files[i].absoluteFilePath().contains(".videoTimeStamps.")){
                hwSyncFileName = files[i].absoluteFilePath();
                qDebug() << "Using for frames: " << files[i].absoluteFilePath();
            }
            if(files[i].suffix().contains("videoTimeStamps")){
                videoTimeStampsFileName = files[i].absoluteFilePath();
                qDebug() << "Using for video timestamps: " << files[i].absoluteFilePath();
            }
        }
        if(hwSyncFileName.isEmpty() || videoTimeStampsFileName.isEmpty()){
            qDebug() << "Error: Video info files missing (videotimestamps or camerahwsync)";
            exit(1);
        }
    }
    if(hwSyncFileName.isEmpty()){
        qDebug() << "Error: HW sync file missing. Please specify it via arguments. Type exporthwtimes --help for more info";
        exit(1);
    }
    if(dioFileName.isEmpty()){
        qDebug() << "Error: DIO binary file missing. Please specify it via arguments. Type exporthwtimes --help for more info";
        exit(1);
    }

}

void DATExportHandler::printHelpMenu(){
    qDebug() << "\nUsed to extract hardware timestamps into the same format as the .videotimestamp file, \n"
                "using camera pulses attached to DIO. Using the DIO channel's extracted binary file and \n"
                "the video files of the recording, will create a separate .HWTimeStamps file containing \n"
                "the camera's hardware timestamps instead of Trodes' software timestamps for each frame. \n";

    qDebug() << "Usages: 1) exporthwtimes -dio </path/to/diopulses.dat>  -hwsync </path/to/videoframes/file> -vidstamps </path/to/.videotimestamps>";
    qDebug() << "        2) exporthwtimes -dio </path/to/diopulses.dat>  -h264 </path/to/h264videofile>";
    qDebug() << "-dio           [Required] Extracted DIO channel file with camera pulses (.dat)\n";
    qDebug() << "-h264          .h264 video file. Base name used to find the other two video files";
    qDebug() << "   Or ";
    qDebug() << "-hwsync        Hardware sync file with frames (.videotimestamps.cameraHWSync)";
    qDebug() << "-vidstamps     Video timestamps file, if recorded before version 1.6.0 (.videotimestamps)";
    exit(0);
}

bool DATExportHandler::parseFields(QString fieldLine){
    filePacketSize = 0;
    QRegExp fieldRegex("<([\\w ]*)>");
    bool containsFrames = false;
    int j = 0;
    while((j=fieldRegex.indexIn(fieldLine, j)) != -1){
        fieldRegex.indexIn(fieldLine, j);
        QStringList field = fieldRegex.capturedTexts()[1].split(' ');
        if(field[0].contains("PosTimestamp", Qt::CaseInsensitive)){
            olderVideoFormat = false;
        }
        if(field[0].contains("frame", Qt::CaseInsensitive)){
            containsFrames = true;
        }
        if(field[1] == "uint32"){
            filePacketSize += 4;
        }
        else if(field[1] == "uint64"){
            filePacketSize += 8;
        }
        ++j;
    }
    return containsFrames;
}

int DATExportHandler::setUpVideoAndFrames(){

    if(!hwSyncFileName.isEmpty()){
        QFile hwSyncFile(hwSyncFileName);
        if(!hwSyncFile.open(QIODevice::ReadOnly)){
            qDebug() << "Error: opening hw sync file " + hwSyncFileName;
            return 1;
        }
        //Read through hwsync file settings
        QString inputline = QString(hwSyncFile.readLine());
        if(!inputline.contains("<Start settings>", Qt::CaseInsensitive)){
            qDebug() << "Error: file does not have proper settings format";
            return 1;
        }
        printf("Parsing hw sync file ...  ");
        //While inputline does not contain 'end settings'
        while(!(inputline=QString(hwSyncFile.readLine())).contains("<End settings>", Qt::CaseInsensitive)){
            //Fields
            if(inputline.contains("Fields: ", Qt::CaseInsensitive)){
                olderVideoFormat = true;
                if(!parseFields(inputline.mid(QString("Fields: ").length()))){
                    qDebug() << "Error: file does not contain frames!";
                    return 1;
                }
                buffer.resize(filePacketSize*2);
            }
        }
        if(hwSyncFile.atEnd()){
            qDebug() << "Error: end of file reached after reading settings";
            return 1;
        }
        qint64 a = hwSyncFile.pos();
        hwSyncFile.close();
        hwSyncFile.setFileName(hwSyncFileName);
        hwSyncFile.open(QIODevice::ReadOnly);
        hwSyncFile.seek(a);

        //Read in frames and (if in file) timestamps
        QDataStream data(&hwSyncFile);
        data.setByteOrder(QDataStream::LittleEndian);
        while(!hwSyncFile.atEnd()){
            quint32 frame;
            quint64 hwa;
            //If this file version has timestamps, read in first
            if(!olderVideoFormat){
                quint32 vidtime;
                data >> vidtime;
                videoTimes.append(vidtime);
            }
            //Then read in frame data.
            data >> frame;
            frames.append(frame);

            //If new version also read in hardware time
            if(!olderVideoFormat){
                data >> hwa;
            }
        }
        hwSyncFile.close();
        printf("Done. \n");

        if(olderVideoFormat){
            printf("Parsing video timestamps ... ");
            QFile videoTSFile;
            videoTSFile.setFileName(videoTimeStampsFileName);
            if(!videoTSFile.open(QIODevice::ReadOnly)){
                qDebug() << "Error: when opening video timestamps file. ";
                return 1;
            }

            while(!(inputline=QString(videoTSFile.readLine())).contains("<End settings>", Qt::CaseInsensitive)); //Skip ahead until after settings line

            data.setDevice(&videoTSFile);
            data.setByteOrder(QDataStream::LittleEndian);
            while(!videoTSFile.atEnd()){
                quint32 vidtime;
                data >> vidtime;
                videoTimes.append(vidtime);
            }
            videoTSFile.close();
            printf("Done. \n");
        }
    }
    return 0;
}

int DATExportHandler::setUpPulses(){
//    qreal avgIncrease = 0.0;
    QString inputline;
    QFile dioFile;
    dioFile.setFileName(dioFileName);
    if(!dioFile.open(QIODevice::ReadOnly)){
        qDebug() << "Error: opening dio .dat file. " << dioFileName;
        return 1;
    }

    printf("Parsing dio .dat file ... ");
    while(!(inputline=QString(dioFile.readLine())).contains("<End settings>", Qt::CaseInsensitive)); //Skip ahead until after settings line
    QDataStream data;
    data.setDevice(&dioFile);
    data.setByteOrder(QDataStream::LittleEndian);

    //Read in data, assuming fields of dio is "<time uint32><state uint8>"
    while(!dioFile.atEnd()){
        quint32 pulsetime = 0;
        quint8 state = 2;
        data >> pulsetime;
        data >> state;
        if(state == 1){
            if(pulseTimes.length()) avgIncrease += pulsetime - pulseTimes.last();
            pulseTimes.append(pulsetime);
        }
    }
    dioFile.close();
    avgIncrease /= (qreal)pulseTimes.length();
    printf("Done. \n");
    return 0;
}

int DATExportHandler::processData(){
    int blinkLength = 500;
    int TIMESTAMPGAP = 30000*((qreal)blinkLength/1000);
    //qDebug() << "Creating "

    /*ASSUMPTIONS:
     * DIO .dat file used is formatted as <time uint32><state uint8>
     * Video hw sync timestamps file is formatted as <PosTimestamp uint32><HWframeCount uint32><HWTimestamp uint64>
     * or formatted as <FrameCount uint32> and .videotimestamps contains <PosTimestamp uint32> numbers
     */

    //Read in frames and video timestamps
    if(setUpVideoAndFrames()){
        return 1;
    }

    //Pulse timestamps
    if(setUpPulses()){
        return 1;
    }

    //Align the two vectors
    //**********************************************************************
    //Currently arbitrarily checking if times differences is over 10000, the gap for synchronization purposes
    printf("Calculating and aligning hardware times with video frames ... ");
    int videoIndex, pulseIndex;
    for(videoIndex = 1; videoIndex < videoTimes.length(); videoIndex++){
        if(frames[videoIndex] == 1 || videoTimes[videoIndex]-videoTimes[videoIndex-1] > TIMESTAMPGAP)
            break;
    }
    for(pulseIndex = 1; pulseIndex < pulseTimes.length(); pulseIndex++){
        if(pulseTimes[pulseIndex]-pulseTimes[pulseIndex-1] > TIMESTAMPGAP)
            break;
    }

    if(videoIndex == videoTimes.length()-1){
        qDebug() << "Error: Video synchronization gap could not be found";
        return 1;
    }
    if(pulseIndex == pulseTimes.length()-1){
        qDebug() << "Error: Pulse synchronization gap could not be found";
        return 1;
    }

    QVector<quint32> finalTimes;
    finalTimes.fill(0, videoTimes.length());

    int delta = pulseIndex - videoIndex; //delta, difference in indices
    //should always be positive since pulses start before camera starts, so sync frame is later in the list

    int skippedFrames = 0;

    //Start at frame 1
    quint32 prevFrame = frames[videoIndex]-1;
    for(int i = videoIndex; i < finalTimes.length(); i++){
        //If frame(s) was skipped, increase skippedFrames counter by that many skipped
        if(frames[i] > prevFrame+1){
            skippedFrames += frames[i]-prevFrame+1;
        }

        //Pulse's index (index of video frames + difference in start index + skipped frames
        int hwIndex = i + delta + skippedFrames;
        if(hwIndex >= pulseTimes.length()){
            //when pulses end before video stamps, estimate timestamp
            finalTimes[i] = finalTimes[i-1] + (quint32)avgIncrease;
        }
        else{
            //rest of the time, get pulsetime[], moving ahead by the delta and skipped frames
            finalTimes[i] = pulseTimes[hwIndex];
        }
        prevFrame = frames[i];
    }

    //Go back and fill in stamps before frame 1
    for(int i = 0; i < videoIndex; i++){
        finalTimes[i] = pulseTimes[i+delta];
    }
    printf("Done. \n");

//    qDebug() << finalTimes.length() << pulseTimes.length();
//    for(int i = 0; i < finalTimes.length(); i++)
//        qDebug() << finalTimes[i] << pulseTimes[i];


    //Write timestamps to file
    //****************************
    //Create output file
    QFile hwTimeStampsFile;
    QString hwstampsfilename = hwSyncFileName.replace(QRegExp(".videoTimeStamps.*"), ".HWTimeStamps");
    hwTimeStampsFile.setFileName(hwstampsfilename);
    if(!hwTimeStampsFile.open(QIODevice::WriteOnly)){
        qDebug() << "Error: creating output file" << hwstampsfilename;
        return 1;
    }
    printf("Writing to output file %s ... ", qPrintable(QFileInfo(hwTimeStampsFile).fileName()));

    hwTimeStampsFile.write("<Start settings>\n");
    hwTimeStampsFile.write("Fields: <PulseTimes uint32>\n");
    hwTimeStampsFile.write("<End settings>\n");
    QDataStream data;
    data.setDevice(&hwTimeStampsFile);
    data.setByteOrder(QDataStream::LittleEndian);
    for(int i = 0; i < finalTimes.length(); i++){
        data << finalTimes[i];
    }
    hwTimeStampsFile.flush();
    printf("Done. \n");
    return 0; //success
}
