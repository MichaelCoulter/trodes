#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    supportedDataCategories = DataCategories::SpikeBand | DataCategories::ContinuousOutput;
    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    //useSpikeFilters = true;

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract data from a raw rec file and save to a file for PHY file format (used by Klusta Suite. \n");
    printf("It will apply whatever notch filter \n and spike band reference settings you have in the workspace by default unless a flag is provided to change the default behavior.\n \n");

    printf("Usage:  exportPHY -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n"
           "    -outputrate -1 (full) \n"
           "    -interp -1 (inf) \n");


    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }
        optionInd++;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "Exporting to PHY format file...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    //createFilters();

    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".phy"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating phy directory.";
            return -1;
        }
    }

    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;



    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    QString infoLine;
    timeFilePtrs.last()->write("<Start settings>\n");
    infoLine = QString("Description: Time stamps\n");
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());

    writeDefaultHeaderInfo(timeFilePtrs.last());


    QString fieldLine;
    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\n";
    timeFilePtrs.last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs.last()->write("<End settings>\n");
    timeFilePtrs.last()->flush();




    //Create an output file for the neural data
    //*****************************************

    //Create an output file for the PHY .dat file
    //This file has the following structure, with no header info:
    //t1_ch1, t1_ch2, .... , t1_chN, t2_ch1 ...
    //Each sample is 16-bit
    //*****************************************
    neuroFilePtrs.push_back(new QFile);
    neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".phy.dat"));
    if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
    neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);



    //Create an output file for the channel info (we may want to change to JSON for this)
    //*****************************************
    QFile* infoFilePtr = new QFile;
    QDataStream* infoStreamPtr;



    infoFilePtr->setFileName(saveLocation+fileBaseName+QString(".info.dat"));
    if (!infoFilePtr->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    infoStreamPtr = new QDataStream(infoFilePtr);
    infoStreamPtr->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file

    infoFilePtr->write("<Start settings>\n");
    infoLine = QString("Description: Channel info for PHY data\n");
    infoFilePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Byte_order: little endian\n");
    infoFilePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Original_file: ") + fi.fileName() + "\n";
    infoFilePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Clock rate: %1\n").arg(hardwareConf->sourceSamplingRate);
    infoFilePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Decimation: %1\n").arg(decimation);
    infoFilePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Number_of_channels: %1\n").arg(channelPacketLocations.length());
    infoFilePtr->write(infoLine.toLocal8Bit());

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<nTrode int16>";
    fieldLine += "<nTrodeChannel int16>";
    fieldLine += "\n";
    infoFilePtr->write(fieldLine.toLocal8Bit());
    infoFilePtr->write("<End settings>\n");

    for (int i = 0; i < channelPacketLocations.length(); i++) {
        *infoStreamPtr << (int16_t)nTrodeForChannels[i] << (int16_t)channelInNTrode[i];
    }

    infoFilePtr->flush();
    infoFilePtr->close();







    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }



        }



        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {

            if (!getNextPacket()) {
                //We have reached the end of the file
                break;
            }


            int fileInd = 0; // all neural data in one file
            if ((currentTimeStamp%decimation) == 0) { //if we are subsampling the data, make sure this is a point we want to save in the first place
                //save data in time file
                *timeStreamPtrs.at(0) << currentTimeStamp; //Write the data to disk

                for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
                    const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::SPIKE);
                    for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                          *neuroStreamPtrs.at(fileInd) << tmpNtrodeData[chInd]; //Write the data to disk
                    }
                }
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStampInFile = currentTimeStamp;

        }

        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }



    return 0; //success
}
