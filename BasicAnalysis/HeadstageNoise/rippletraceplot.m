filename = '20151126_templeton_300uV_stim1m_ripch6.rec';
directory = '/opt/data36/daliu/trodes/templeton/';




channel = [6,1];

import_trodes = readTrodesFileContinuous(strcat(directory, filename),channel,0);

timestamp = import_trodes.timestamps;
rawData = import_trodes.channelData;

%%

[b,a] = butter(4,[150,250]/(30000/2));
ripFiltData = fliplr(filter(b,a,fliplr(rawData)));

plot(timestamp,rawData, 'r');
hold on;
plot(timestamp,ripFiltData, 'b');
hold off;
